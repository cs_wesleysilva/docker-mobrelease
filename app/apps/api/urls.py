# -*- coding: utf-8 -*-
from django.conf.urls.defaults import patterns, include, url

from apps.api.v1 import *

urlpatterns = patterns('',
    (r'^', include(v1_api.urls)),
)
